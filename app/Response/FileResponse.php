<?php
namespace App\Response;

use Avris\Micrus\Controller\Http\CookieBag;
use Avris\Micrus\Controller\Http\Response;

class FileResponse extends Response
{
    /** @var string */
    protected $path;

    /**
     * @param string $path
     * @param string|bool $download
     * @param CookieBag|null $cookies
     */
    public function __construct($path, $download = false, CookieBag $cookies = null)
    {
        $this->path = $path;

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $headers = [
            'Content-Type' => finfo_file($finfo, $path),
            'Content-Length' => (string) filesize($path),
        ];
        finfo_close($finfo);

        if ($download) {
            $filename = is_string($download) ? $download : basename($path);
            $headers['Content-Disposition'] = 'attachment; filename="'.addslashes($filename).'"';
            $headers['Pragma'] = 'public';
            $headers['Cache-Control'] = 'must-revalidate, post-check=0, pre-check=0';
        }

        parent::__construct(file_get_contents($path), 200, $headers);
        $this->setCookies($cookies);
    }
}
