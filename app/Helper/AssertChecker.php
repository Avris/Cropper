<?php
namespace App\Helper;

use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Model\Assert\Assert;

class AssertChecker
{
    /**
     * @param mixed $value
     * @param Assert[] $asserts
     * @throws InvalidArgumentException
     */
    public static function check($value, $asserts)
    {
        foreach ($asserts as $assert) {
            $message = $assert->validate($value);
            if ($message !== true) {
                throw new InvalidArgumentException(
                    $message ?: l('validator.' . $assert->getName(), $assert->getReplacements())
                );
            }
        }
    }
}
