$footer = $('footer')
$window = $(window)
$(window).resize ->
  $footer.removeClass('fixed')
  footerBottom = $footer.outerHeight() + $footer.position().top + parseInt($footer.css('margin-top').replace('px', ''))
  $footer.toggleClass('fixed', footerBottom < $window.height())
.trigger('resize')
