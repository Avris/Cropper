$.fn.scrollTo = (offset = 16) ->
  $('body').animate({scrollTop: ($(this).position().top-offset)+"px"},1000)
  this
