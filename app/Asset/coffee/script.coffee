uid = makeUid(16)

$well = $('.well')
$step1 = $('.step1')
$uploadBtn = $step1.find('.btn-success')
$fromUrl = $('.fromUrl')
$fromUrlInput = $fromUrl.find('input')
$fromUrlBtn = $fromUrl.find('button')
$step2 = $('.step2')
$cropBox = $('img.cropBox')
$step3 = $('.step3')
$cropBtn = $('.btn-crop')
$restartBtn = $('.btn-restart')
$result = $('img.cropResult')
$downloadBtn = $('.btn-download')

croppingSize = undefined
zoom = undefined

showError = (message) ->
  alert = $ """
<div class='alert alert-dismissable alert-danger'>
  <button type='button' class='close' data-dismiss='alert'>&times;</button>
  <strong>#{M.l('error.label')}:</strong> #{message}
</div>
"""
  $well.after alert
  setTimeout(
    -> alert.hide('slow')
    5000
  )

uploader = new plupload.Uploader
  runtimes: 'html5,flash,silverlight,html4'
  browse_button: $uploadBtn[0]
  drop_element: $step1[0]
  url: M.route('upload', {uid: uid})
  flash_swf_url: M.asset('assetic/plupload/Moxie.swf')
  silverlight_xap_url: M.asset('lib/plupload/Moxie.xap')
  multi_selection: false
  filters:
    mime_types: [{
      title: 'Zdjęcia'
      extensions: 'jpg,jpeg,gif,png'
    }]
  init:
    FilesAdded: (up, files) ->
      return if $uploadBtn.hasClass('disabled')
      $uploadBtn.spinner('start').addClass('disabled')
      uploader.start()
    FileUploaded: (up, file, info) ->
      $uploadBtn.spinner('stop').removeClass('disabled')
      res = JSON.parse(info.response)
      if res.error?
        showError(res.error.message)
      else
        $cropBox.attr('src', M.route('display', {uid: uid, folder: 'input'}))
    Error: (up, err) -> showError(err.message)
uploader.init()

fetchFromUrl = ->
  if not $fromUrlInput[0].validity.valid
    message =
      if $fromUrlInput[0].validity.valueMissing then M.l('validator.NotBlank')
      else if $fromUrlInput[0].validity.typeMismatch then M.l('validator.Url')
      else $fromUrlInput[0].validationMessage
    return showError(message)

  return if $fromUrlBtn.hasClass('disabled')
  $fromUrlInput.prop('disabled', true)
  $fromUrlBtn.addClass('disabled')
  $fromUrlBtn.spinner('start')

  $.post M.route('fromUrl', {uid: uid}),
    {url: $fromUrlInput.val()}
    (data) ->
      $fromUrlInput.prop('disabled', false)
      $fromUrlBtn.removeClass('disabled')
      $fromUrlBtn.spinner('stop')
      $fromUrlInput.val('')
      if data.error?
        showError(data.error.message)
      else
        $cropBox.attr('src', M.route('display', {uid: uid, folder: 'input'}))
    'json'

$fromUrlBtn.click fetchFromUrl
$fromUrlInput.keydown (e) ->
  fetchFromUrl() if e.keyCode == 13

$step1.on 'drop', (e) ->
  $step1.removeClass('dragover')
  url = e.originalEvent.dataTransfer.getData('url')
  if url
    $fromUrlInput.val(url)
    fetchFromUrl()
    false

$('body').on 'dragstart dragenter', ->
  $step1.addClass('dragover')
.on 'dragend drop', ->
  $step1.removeClass('dragover')

setSize = (c) ->
  $cropBtn.toggleClass('disabled', !parseInt(c.w))
  croppingSize = c.map((v) -> v * zoom)

$cropBox.on 'error', ->
  showError M.l('error.imageLoad')
.on 'load', ->
  $step1.hide('slow')
  $step2.show('slow', ->
    JcropLoad()
    $step2.scrollTo()
  )

$cropBtn.click ->
  return if $cropBtn.hasClass('disabled')
  $cropBtn.addClass('disabled')
  $cropBtn.spinner('start')
  $.post M.route('crop', {uid: uid}),
    {croppingSize: croppingSize}
    (data) ->
      $cropBtn.removeClass('disabled')
      $cropBtn.spinner('stop')
      if data.error?
        showError(data.error.message)
      else
        $result.attr('src', M.route('display', {uid: uid, folder: 'output'}))
        $downloadBtn.attr('href', M.route('display', {uid: uid, folder: 'output', download: 1}))
    'json'

$result.on 'error', ->
  showError M.l('error.imageLoad')
.on 'load', ->
  $step2.addClass('col-lg-6')
  $step3.show('slow').scrollTo(36)

$restartBtn.click ->
  uid = makeUid(16)
  $step1.show('slow')
  $step2.removeClass('col-lg-6').hide('slow')
  $step3.hide('slow')
  uploader.settings.url = M.route('upload', {uid: uid})
  JcropDestroy()

$(window).resize ->
  JcropDestroy()
  JcropLoad()

JcropLoad = ->
  zoom = $cropBox[0].naturalWidth / $cropBox.width()
  $cropBox.Jcrop
    onChange: setSize
    onSelect: setSize
    aspectRatio: 35 / 45
    setSelect: [0, 0, 1000, 1000]
    minSize: [73, 157]
JcropDestroy = ->
  $cropBox.data('Jcrop')?.destroy()
  $cropBox.css('width', '')
  $cropBox.css('height', '')
