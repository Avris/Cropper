@makeUid = (len, chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') ->
  (chars.charAt(Math.floor(Math.random() * chars.length)) for i in [1..len]).join('')
