$.fn.spinner = (action) ->
  t = $(this)
  data = 'spinnerOriginal'

  start = -> t.data(data, t.html()).html('<span class="fa fa-spinner fa-spin"></span>'); t
  stop = -> t.html(t.data(data)).removeData(data); t

  switch action
    when 'start' then return start()
    when 'stop' then return stop()
    else return if t.data(data) then stop() else start()
