Object.defineProperty(Object.prototype, 'map',
  value: (f, ctx) ->
    ctx = ctx or this;
    self = this
    result = {}
    Object.keys(self).forEach((k) ->
      result[k] = f.call(ctx, self[k], k, self)
    )
    result
)
