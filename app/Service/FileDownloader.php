<?php
namespace App\Service;

use Avris\Micrus\Controller\Http\UploadedFile;

class FileDownloader
{
    /** @var string */
    protected $rootDir;

    /**
     * @param string $rootDir
     */
    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
    }

    public function getSize($url)
    {
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        $data = curl_exec($curl);
        curl_close($curl);

        if ($data && preg_match_all('/^HTTP\/[12]\.[01] (\d\d\d)/m', $data, $matches)) {
            if ((int) end($matches[1]) == 200 && preg_match('/Content-Length: (\d+)/', $data, $matches)) {
                return (int) $matches[1];
            }
        }

        return false;
    }

    public function download($url)
    {
        $tmpPath = tempnam(sys_get_temp_dir(), 'Micrus');

        $ch = curl_init($url);
        $fp = fopen($tmpPath, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        if (!curl_exec($ch)) {
            return false;
        }
        curl_close($ch);
        fclose($fp);

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $contentType = finfo_file($finfo, $tmpPath);
        finfo_close($finfo);

        return new UploadedFile([
            'name' => basename($url),
            'type' => $contentType,
            'tmp_name' => $tmpPath,
            'error' => UPLOAD_ERR_OK,
            'size' => filesize($tmpPath)
        ], $this->rootDir);
    }
}
