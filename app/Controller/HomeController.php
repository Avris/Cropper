<?php
namespace App\Controller;

use App\Helper\AssertChecker;
use App\Response\FileResponse;
use App\Service\FileDownloader;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Annotations as M;
use Avris\Micrus\Controller\Http\JsonResponse;
use Avris\Micrus\Controller\Http\UploadedFile;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Exception\NotFoundException;
use Avris\Micrus\Model\Assert as Assert;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;

class HomeController extends Controller
{
    /**
     * @M\Route("/")
     */
    public function homeAction()
    {
        return $this->render();
    }

    /**
     * @M\Route("/upload/{uid}", name="upload", methods={"POST"}, requirements={"uid": "[A-Za-z0-9]{16}"})
     */
    public function uploadAction($uid)
    {
        try {
            $this->handleFile($this->getFiles('file'), $uid);

            return $this->buildRpcResponse($uid, 'OK');
        } catch (\Exception $e) {
            return $this->buildRpcResponse($uid, $e);
        }
    }

    /**
     * @M\Route("/fromUrl/{uid}", name="fromUrl", methods={"POST"}, requirements={"uid": "[A-Za-z0-9]{16}"})
     */
    public function fromUrlAction($uid)
    {
        try {
            $url = $this->getData('url');

            AssertChecker::check($url, [new Assert\NotBlank, new Assert\Url]);

            /** @var FileDownloader $fileDownloader */
            $fileDownloader = $this->getService('fileDownloader');

            $size = $fileDownloader->getSize($url);
            if (!$size) {
                throw new InvalidArgumentException(l('validator.File.File'));
            } else if ($size > 5 * 1024 * 1024) {
                throw new InvalidArgumentException(l('validator.File.MaxSize', ['%value%' => '5.0 MB']));
            }

            $this->handleFile($fileDownloader->download($url), $uid);

            return $this->buildRpcResponse($uid, 'OK');
        } catch (\Exception $e) {
            return $this->buildRpcResponse($uid, $e);
        }
    }

    /**
     * @M\Route("/display/{folder}/{download}/{uid}",
     *     name="display",
     *     requirements={"uid": "[A-Za-z0-9]{16}", "folder": "input|output", "download": "0|1"},
     *     defaults={"download": "0"}
     * )
     */
    public function displayAction($folder, $download, $uid)
    {
        $files = glob(sprintf('%s/run/fotos/%s/%s.*', $this->getRootDir(), $folder, $uid));
        if (count($files) !== 1) {
            throw new NotFoundException;
        }

        return new FileResponse($files[0], (bool) $download);
    }

    /**
     * @M\Route("/crop/{uid}", name="crop", requirements={"uid": "[A-Za-z0-9]{16}"})
     */
    public function cropAction($uid)
    {
        $size = $this->getData('croppingSize');

        $files = glob($this->getRootDir() . '/run/fotos/input/' . $uid . '.*');
        if (count($files) !== 1) {
            throw new NotFoundException;
        }

        $imagine = new Imagine();
        $source = $imagine->open($files[0]);

        $cropped = $source->crop(
            new Point((int) $size['x'], (int) $size['y']),
            new Box((int) $size['w'], (int) $size['h'])
        )->resize(new Box(413, 531));

        $target = $imagine->create(new Box(1063, 1535));
        $target->paste($cropped, new Point(73, 157));
        $target->paste($cropped, new Point(2*73+413, 157));
        $target->paste($cropped, new Point(73, 2*157+531));
        $target->paste($cropped, new Point(2*73+413, 2*157+531));

        $target->save($this->getRootDir() . '/run/fotos/output/' . $uid . '.jpg');

        return $this->buildRpcResponse($uid, 'OK');
    }

    protected function handleFile(UploadedFile $file, $uid)
    {
        AssertChecker::check($file, [
            new Assert\File\File,
            new Assert\File\Image,
            new Assert\File\Type(['image/jpeg','image/jpg','image/pjpeg','image/x-png','image/png','image/gif']),
            new Assert\File\Extension(['gif', 'jpeg', 'jpg', 'png']),
            new Assert\File\MaxSize('5M'),
            new Assert\File\MinWidth(73),
            new Assert\File\MinHeight(157),
        ]);

        $file->moveTo(sprintf('run/fotos/input/%s.%s', $uid, $file->getExtension()));
    }

    protected function buildRpcResponse($uid, $result)
    {
        $data = [
            'jsonrpc' => '2.0',
            'id' => $uid,
        ];

        if ($result instanceof \Exception) {
            $data['error'] = [
                'code' => 100,
                'message' => $result instanceof InvalidArgumentException
                    ? $result->getMessage()
                    : l('error.internal'),
            ];
        } else {
            $data['result'] = $result;
        }

        return new JsonResponse($data);
    }
}
